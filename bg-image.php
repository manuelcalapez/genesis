<?php

//grab an Advanced Custom Fields sub-field and display it as a background image on a div


add_action( 'genesis_after_header', 'tcc_custom_header' );

function tcc_custom_header(){

	echo '<div class="tcc_image" style="background-image: url(' . get_sub_field('image') . ')"></div>';
	
}