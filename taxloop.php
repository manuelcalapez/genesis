<?php

add_action( 'genesis_loop', 'tcc_custom_tax_loop' );

function tcc_custom_tax_loop(){
	if(is_page()){
		
		$argsCustPost = array(
			'post_type' => 'post',
			'tax_query' => array(
				array(
					'taxonomy' => 'people',
					'field'    => 'slug',
					'terms'    => 'bob',
				),
			),
		);
		$query = new WP_Query( $args );
		
		$loopCT = new WP_Query( $argsCustPost );
		if( $loopCT->have_posts() ) {

			// loop through posts
			echo '<section class="__yourclass__">';
			
				while( $loopCT->have_posts() ): $loopCT->the_post();

					echo '<article class="__articleclass__">';
			
						echo '<h4 class="__titleclass__">'. get_the_title() . '</h4>';
						echo '<div class="__divclass__">' . get_the_post_thumbnail($post->id, 'medium_large'). '</div>';

					echo '</article>';

				endwhile;
			echo '</section>';
		}
	}
}