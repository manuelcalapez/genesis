<?php

add_action( 'genesis_loop', 'tcc_custom_post_loop' );

function tcc_custom_post_loop(){
	if(is_page()){
		
		$argsCustPost = array(
			'post_type' => '__cptSlug__',
			'orderby' => 'date',
			'order' => 'DESC',
			'posts_per_page' => -1,
		);
		
		$loopCT = new WP_Query( $argsCustPost );
		if( $loopCT->have_posts() ) {

			// loop through posts
			echo '<section class="__yourclass__">';
			
				while( $loopCT->have_posts() ): $loopCT->the_post();

					echo '<article class="__articleclass__">';
			
						echo '<h4 class="__titleclass__">'. get_the_title() . '</h4>';
						echo '<div class="__divclass__">' . get_the_post_thumbnail($post->id, 'medium_large'). '</div>';

					echo '</article>';

				endwhile;
			echo '</section>';
		}
	}
}